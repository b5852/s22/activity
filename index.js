
// Users Collection

{

	"_id":"user001", //system generated
	"firstName": "John",
	"lastName": "Smith",
	"email": "johnsmith@mail.com",
	"password": "johnsmith123",
	"isAdmin": true,
	"mobileNumber": "09123456789",
	"dateTimeRegistered": "2022-06-10T15:00:00.007" //dateTime datatype

}

{

	"_id":"user002", //system generated
	"firstName": "Jane",
	"lastName": "Doe",
	"email": "janedoe@mail.com",
	"password": "janedoe123",
	"isAdmin": false,
	"mobileNumber": "09123123123",
	"dateTimeRegistered": "2022-06-10T15:00:00.007" //dateTime datatype

}

// Orders Collection 

{

	"_id":"order001", //system generated
	"userID": ["user002"],
	"transactionDate": "2022-06-10T15:00:00.007", //dateTime datatype
	"status": "Complete",
	"total": 1000

}


// Order Products Collection

{

	"_id":"orderProduct001", //system generated
	"orderID": ["order001"],
	"productID": ["product001"],
	"quantity": 1,
	"price": 1000,
	"subTotal": 1000

}


// Products Collection

{

	"_id":"product001", //system generated
	"name": "Power Bank",
	"description": "Use to store power electricity.",
	"price": 1000,
	"stocks": 9,
	"isActive": true,
	"SKU": "ABCD-1234"

}